Name:           gnome-console
Version:        42~beta
Release:        %autorelease
Summary:        Simple user-friendly terminal emulator for the GNOME desktop
License:        GPLv3+
URL:            https://gitlab.gnome.org/GNOME/console
Source:         https://download.gnome.org/sources/gnome-console/42/gnome-console-%{version_no_tilde .}.tar.xz

# https://gitlab.gnome.org/GNOME/console/-/merge_requests/86
Patch0:          0001-Enable-Ctrl-PgUp-PgDn-to-switch-tabs-if-there-are-mu.patch
Patch1:			opacity.diff

BuildRequires:  meson >= 0.59.0
BuildRequires:  gcc
BuildRequires:  pkgconfig(gio-2.0) >= 2.66
BuildRequires:  pkgconfig(gtk+-3.0) >= 3.24
BuildRequires:  pkgconfig(libhandy-1) >= 1.5
BuildRequires:  pkgconfig(vte-2.91) >= 0.67
BuildRequires:  pkgconfig(libgtop-2.0)
BuildRequires:  pkgconfig(gsettings-desktop-schemas)
BuildRequires:  /usr/bin/sassc
BuildRequires:  /usr/bin/desktop-file-validate
BuildRequires:  /usr/bin/appstreamcli
# Nautilus integration
BuildRequires:  pkgconfig(libnautilus-extension)
BuildRequires:  pkgconfig(gio-unix-2.0)

%description
%{summary}.

%package nautilus
Summary:        Nautilus integration with %{name}
Requires:       %{name}%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}
Supplements:    (nautilus and %{name})

%description nautilus
%{summary}.

%prep
%autosetup -p 1 -n %{name}-%{version_no_tilde .}
# All these are handled by the RPM filetriggers
# … and -Werror is just terrible
sed -i -r -e '/(werror=|glib_compile_schemas|gtk_update_icon_cache|update_desktop_database)/s/true/false/' meson.build

%build
%meson -D nautilus=enabled -D sassc=enabled
%meson_build

%install
%meson_install
%find_lang kgx

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/org.gnome.Console.desktop
appstreamcli validate --no-net %{buildroot}%{_metainfodir}/org.gnome.Console.metainfo.xml

%files -f kgx.lang
%{_bindir}/kgx
%{_datadir}/applications/org.gnome.Console.desktop
%{_metainfodir}/org.gnome.Console.metainfo.xml
%{_datadir}/glib-2.0/schemas/org.gnome.Console.gschema.xml
%{_datadir}/dbus-1/services/org.gnome.Console.service
%{_datadir}/icons/hicolor/scalable/apps/org.gnome.Console.svg
%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.Console-symbolic.svg

%files nautilus
%{_libdir}/nautilus/extensions-3.0/libkgx-nautilus.so

%changelog
%autochangelog
